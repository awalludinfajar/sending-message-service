#! /bin/bash

which ssh-agent || ( apk update && apk add openssh-client )
eval $(ssh-agent -s)
mkdir -p ~/.ssh
echo "$SSH_INSTANCE_TOKEN" | tr -d '\r' > ~/.ssh/test.pem
chmod 600 ~/.ssh/test.pem 

USER_SSH="ubuntu"
URL_SSH="ec2-54-255-210-62.ap-southeast-1.compute.amazonaws.com"

TIBCOURL="ec2-13-250-54-242.ap-southeast-1.compute.amazonaws.com"
TIBCOPORT="7222"

IMAGE_NAME="tibco-automation-ci-cd"

CONTAINER_STOP="sudo docker stop "$IMAGE_NAME" "
CONTAINER_REMOVE="sudo docker container rm "$IMAGE_NAME" "

FIRST_SSH="sudo docker pull awalludinfajar/sample-tibco-springboot:\"$CI_COMMIT_SHA\""
SECOND_SSH=""$CONTAINER_STOP" && "$CONTAINER_REMOVE" && sudo docker run -e TIBCOEMSURL="$TIBCOEMSURL" -e TIBCOEMSPORT="$TIBCOPORT" -p 3200:8080 --name "$IMAGE_NAME" -d awalludinfajar/sample-tibco-springboot:\"$CI_COMMIT_SHA\" "

ssh -T -o StrictHostKeyChecking=no -i ~/.ssh/test.pem $USER_SSH@$URL_SSH "$FIRST_SSH"
ssh -T -o StrictHostKeyChecking=no -i ~/.ssh/test.pem $USER_SSH@$URL_SSH "$SECOND_SSH"
