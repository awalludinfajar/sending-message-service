package com.topic_tibco.topic.controller;

import javax.jms.JMSException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.topic_tibco.topic.model.SendMessage;
import com.topic_tibco.topic.service.TibcoService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.GetMapping;


@RestController
public class SampleTopicController {
    
    @Autowired
    private TibcoService tibcoService;

    // I m updated for testing docker build image in gitlab e.getMessage()
    @PostMapping(path = "/api/send/message", produces = MediaType.APPLICATION_JSON_VALUE)
    public SendMessage produceMessageTopic(@RequestBody SendMessage message) throws JMSException, JsonProcessingException, InterruptedException {

        tibcoService.sendQueue(message);
        
        return message;
    }

    @GetMapping("/api/send/test")
    public String sampleShow() {
        System.out.println("Show Sample Test Test Message");
        return null;
    }
}
