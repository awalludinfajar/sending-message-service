package com.topic_tibco.topic.service;

import java.util.Random;

import javax.jms.JMSException;
import javax.jms.TextMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.topic_tibco.topic.config.QueueConfiguration;
import com.topic_tibco.topic.config.TopicConfiguration;
import com.topic_tibco.topic.model.SendMessage;

@Service
public class TibcoService {

    @Autowired
    private TopicConfiguration topicConfiguration;

    @Autowired
    private QueueConfiguration queueConfiguration;

    @Autowired
    private ObjectMapper objectMapper;

    public void sendTopic(String msg) throws JMSException {
        TextMessage message = topicConfiguration.sessionFactory().createTextMessage(msg);
        topicConfiguration.producer().send(message);
    }

    public void sendQueue(SendMessage msg) throws JMSException, JsonProcessingException, InterruptedException {
        // String converted = objectMapper.writeValueAsString(msg);
        // System.out.println(converted); SendMessage randInt.nextInt(0,100)
        Random randInt = new Random();
        for (int i = 0; i < 100; i++) {
            TextMessage message = queueConfiguration.sessionFactory().createTextMessage("{\"name\":\""+msg.name+"\", \"address\":\""+msg.address+"\",\"iterations\":\""+i+"\"}");
            queueConfiguration.setup().send(message);
            Thread.sleep(100);
        }

        queueConfiguration.setup().close();
        queueConfiguration.sessionFactory().close();
        // TextMessage message = queueConfiguration.sessionFactory().createTextMessage(msg);
    }
};