package com.topic_tibco.topic;

// import javax.jms.Connection;
// import javax.jms.ConnectionFactory;
// import javax.jms.JMSException;
// import javax.jms.MessageProducer;
// import javax.jms.Session;
// import javax.jms.TextMessage;
// import javax.jms.Topic;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
// import org.springframework.jms.annotation.JmsListener;

@SpringBootApplication
public class TopicApplication {

	public static void main(String[] args) {

		SpringApplication.run(TopicApplication.class, args);

		// send message to tibco topic
		// try {
		// 	ConnectionFactory factory = new com.tibco.tibjms.TibjmsConnectionFactory("tcp://localhost:7222");
		// 	Connection connection = factory.createConnection("admin", "");
		// 	Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

		// 	Topic topic = session.createTopic("sampleTopic");
		// 	MessageProducer producer = session.createProducer(topic);
		// 	TextMessage message = session.createTextMessage("Hello, this is second Message from topic!");
		// 	producer.send(message);

		// 	producer.close();
		// 	session.close();
		// 	connection.close();
		// } catch (JMSException e) {
		// 	e.printStackTrace();
		// }
	}
}
