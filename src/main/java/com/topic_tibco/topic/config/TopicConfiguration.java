package com.topic_tibco.topic.config;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.MessageConsumer;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.Topic;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.annotation.EnableJms;

@Configuration
@EnableJms
public class TopicConfiguration {

    @Value("${ems.port}")
	private String port;

	@Value("${ems.server}")
	private String server;

	@Value("${ems.user}")
	private String user;

    @Value("${ems.topics}")
    private String topic;

    public ConnectionFactory setConnectionFactory() throws JMSException {
        ConnectionFactory factory = new com.tibco.tibjms.TibjmsConnectionFactory("tcp://" + server + ":" + port);
        factory.createConnection(user,"");

        return factory;
    }

    // Setup for connection a session and also create session too
    public Session sessionFactory() throws JMSException {
        Connection connect = setConnectionFactory().createConnection();
        Session session = connect.createSession(false, Session.AUTO_ACKNOWLEDGE);
        
        return session;
    }

    // Setup for producer connection to send messages
    public MessageProducer producer() throws JMSException {
        Topic topics = sessionFactory().createTopic(topic);
        MessageProducer producer = sessionFactory().createProducer(topics);

        return producer;
    }
}
