# Definition Arguments
#ARG urlems=192.168.89.70
ARG urlems=ec2-52-221-224-158.ap-southeast-1.compute.amazonaws.com
ARG portems=7222

#Using Maven for base image
FROM maven:3.8.4-openjdk-17 as build

#Make folder app for working directory
RUN mkdir /app
WORKDIR /app

#copy the lib folder
COPY lib /app/lib

#Make folder bin for stored tibco and jms jar file
RUN mvn install:install-file -DgroupId=com.tibco -DartifactId=tibjms -Dversion=8.38.3 -Dpackaging=jar -Dfile=lib/tibjms.jar
RUN mvn install:install-file -DgroupId=javax.jms -DartifactId=jms -Dversion=2.0 -Dpackaging=jar -Dfile=lib/jms-2.0.jar

#Copy project files into the container
COPY pom.xml .
COPY src ./src

# Generating ARG as a argument
ARG urlems
ARG portems

# Setup ENV variable
ENV TIBCOEMSURL=urlems
ENV TIBCOEMSPORT=portems

#Running for building the application
RUN mvn clean install

#Using a smaller Jre-based image for the final image
FROM openjdk:17-alpine

#Set the working directory in the container
WORKDIR /app

#Copy the Jar file into final stage
COPY --from=build /app/target/topic-0.0.1-SNAPSHOT.jar topic-0.0.1-SNAPSHOT.jar

#Exposed app port application will run on
EXPOSE 8080

#Specify the command to run on container start
CMD [ "java", "-jar", "topic-0.0.1-SNAPSHOT.jar" ]

#syintax for build images :  docker build --build-arg urlems=192.168.89.70 --build-arg portems=7222 -t tibco_docker_sample:0.0.1 .
#syintax for running container : docker run -e TIBCOEMSURL=192.168.89.70 -e TIBCOEMSPORT=7222 -p 3200:8080 tibco_docker_sample:0.0.1
